# xrandr-gui

Some xrandr config viewing application which aims to show you more than you are used to by similar apps.
  
Start with `python main.py`
  
**Keyboard functions**  
**t**: hide/show text overlay
  
## TODO
- Option to view colour overlays for different monitor states (or custom colour configuration)
- Port to C
- Use libXrandr (https://stackoverflow.com/a/17037191)
- Do not use a scrollarea
- zooming in should be further possible
- Option to turn off screenshot showing
- Execute xrandr commands (Re-arrange windows in GUI by dragging)
- GUI part to presicely position a monitor by pixel
- Get current xrandr configuration as a command
- Use XComposite for live images
- Maybe show relative window position as well + their names and dimensions (like https://i.stack.imgur.com/bRnp2.png)
- Option to hide monitors from the view
- Scrollwheel support to zoom in and out
- Middle click mouse button hold to move in scrolled area
- Option for transparent or custom background
- Save and load configurations
- Switch to remote machines or local sessions seats when e.g. ZapdoHeads is used.
- Option to add notes to monitors and windows which either show as overlay or are accessable vie a small icon
- Option to start VNC server for specific (future or current) monitors, areas, everything etc.
- Option to add remote monitors via e.g. VNC
