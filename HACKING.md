Monolithic main file used to be `monitor_gui.py`  
Now the main file is `main.py` and the relationship is like that (previous line calls its next):
- main.py
- monitor_gui.py (contains key shortcuts & updating GUI with new xrandr info)
- canvas_helper.py (drawing to GUI)
- monitor_utils.py (currently xrandr output parsing)
