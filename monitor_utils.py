import re
import subprocess
import pyautogui

def get_monitors():
    output = subprocess.check_output(['xrandr', '--listmonitors'], text=True)
    monitors = []
    for line in output.split('\n'):
        match = re.match(r'^\s*\d+:\s*(\+?)(\S+)\s+(\d+)\/\d+x(\d+)\/\d+\+(\d+)\+(\d+)\s+(\S+)$', line)
        if match:
            primary, name, width, height, x, y, _ = match.groups()
            name = name.replace("*", "") if primary == '+' else name
            width, height, x, y = int(width), int(height), int(x), int(y)
            screenshot = pyautogui.screenshot(region=(x, y, width, height)).convert('RGB')
            monitors.append((name, width, height, x, y, screenshot))
            print(f"Found monitor: {name}")
    return monitors

def get_monitor_info(name):
    p1 = subprocess.Popen(['xrandr'], stdout=subprocess.PIPE, text=True)
    p2 = subprocess.Popen(['grep', name], stdin=p1.stdout, stdout=subprocess.PIPE, text=True)
    p1.stdout.close()
    output, _ = p2.communicate()
    fill, dimensions = ('red', '') if 'disconnected' in output else ('green', re.findall(r"\) (.+)", output)[0])
    return fill, dimensions, output
