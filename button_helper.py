import tkinter as tk
from canvas_helper import redraw_rectangles

def handle_events(frame, char):
    if char == 'r':
        frame.update_monitors()
        redraw_rectangles(frame)
    elif char == '+':
        frame.rect_size = frame.rect_size + 1
        redraw_rectangles(frame)
    elif char == '-':
        frame.rect_size = frame.rect_size - 1
        redraw_rectangles(frame)

def create_buttons(frame):
    frame.pack(side=tk.BOTTOM, padx=10, pady=10)
    frame.focus_set()
    tk.Button(frame, text="Refresh (r)", command=lambda: handle_events(frame, 'r')).grid(row=2, column=0, sticky="w")
    tk.Button(frame, text="Zoom (+)", command=lambda: handle_events(frame, '+')).grid(row=2, column=1)
    tk.Button(frame, text="Zoom (-)", command=lambda: handle_events(frame, '-')).grid(row=2, column=2)

def on_key_press(frame, event):
    handle_events(frame,event.char)
