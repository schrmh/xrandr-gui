import tkinter as tk
from PIL import Image, ImageTk

def create_widgets(frame):
    frame.canvas = tk.Canvas(frame, width=800, height=600)
    frame.canvas.create_rectangle(0, 0, 10000, 10000, fill="green")
    frame.canvas.grid(row=0, column=0)
    scroll_x = tk.Scrollbar(frame, orient="horizontal", command=frame.canvas.xview)
    scroll_x.grid(row=1, column=0, sticky="ew")
    scroll_y = tk.Scrollbar(frame, orient="vertical", command=frame.canvas.yview)
    scroll_y.grid(row=0, column=1, sticky="ns")
    frame.canvas.configure(yscrollcommand=scroll_y.set, xscrollcommand=scroll_x.set)
    # set the scrollregion to the bounding box of all items on the canvas
    frame.canvas.configure(scrollregion=frame.canvas.bbox("all"))
    # get initial monitor information
    frame.update_monitors()
    redraw_rectangles(frame)

def redraw_rectangles(frame):
    # clear canvas
    frame.canvas.delete(tk.ALL)

    # redraw rectangles for each monitor
    for i, monitor in enumerate(frame.monitors):
        name, width, height, x, y, screenshot = monitor
        fill, dimensions, output = frame.monitor_info[name]
        rect_x, rect_y, rect_width, rect_height = [i // frame.rect_size for i in (x, y, width, height)]
        img_tk = ImageTk.PhotoImage(screenshot.resize((rect_width, rect_height)))
        frame.canvas.create_image(rect_x, rect_y, anchor=tk.NW, image=img_tk)
        frame.canvas.create_rectangle(rect_x, rect_y, rect_x + rect_width, rect_y + rect_height, outline=fill)
        
        if frame.show_text:
            # Draw outlined text
            text = f"{name}\n({width}x{height})\n{dimensions}"
            draw_text_with_outline(frame.canvas, rect_x + rect_width//2, rect_y + rect_height//2, text)
            
            # Draw outlined special text
            special_text = 'NC' if 'disconnected' in output else '★' if i == 0 else ''
            if special_text:
                draw_text_with_outline(frame.canvas, rect_x, rect_y, special_text, font=("Helvetica", 14, "bold"), anchor=tk.NW)

        # store the image to prevent garbage collection
        setattr(frame, f"img_{i}", img_tk)

def draw_text_with_outline(canvas, x, y, text, font=("Helvetica", 10), fill="black", outline="white", outline_width=1, anchor=tk.CENTER):
    # Draw outline by drawing text multiple times around the main text
    for dx in (-outline_width, 0, outline_width):
        for dy in (-outline_width, 0, outline_width):
            if dx != 0 or dy != 0:
                canvas.create_text(x + dx, y + dy, text=text, font=font, fill=outline, anchor=anchor)
    # Draw main text
    canvas.create_text(x, y, text=text, font=font, fill=fill, anchor=anchor)

