import tkinter as tk
from canvas_helper import create_widgets, redraw_rectangles
from button_helper import on_key_press, create_buttons

class MonitorGUI(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.pack()

        # initialize instance variables
        self.monitors = []
        self.monitor_info = {}
        self.rect_size = 10
        self.show_text = True  # Variable to track text visibility

        # create canvas widgets
        create_widgets(self)

        self.master.bind("<Key>", self.on_key_press)
        create_buttons(self)

    def on_key_press(self, event):
        if event.char == 't':
            self.show_text = not self.show_text
            redraw_rectangles(self)
        else:
            on_key_press(self, event)

    def update_monitors(self):
        from monitor_utils import get_monitors, get_monitor_info
        window_pos = self.master.winfo_x(), self.master.winfo_y()
        self.master.withdraw()  # hide the window
        self.monitors = get_monitors()
        self.master.geometry(f"+{window_pos[0]}+{window_pos[1]}")
        self.master.deiconify()  # show the window again
        self.monitor_info = {}
        for name, _, _, _, _, _ in self.monitors:
            fill, dimensions, output = get_monitor_info(name)
            self.monitor_info[name] = (fill, dimensions, output)

